module Main where
import Data.Complex
import Numeric.LinearAlgebra.Data
import Control.Monad
import System.Environment
import Graphics.Gloss

type Pos = (Double, Double, Bool)
type Field = [[Pos]]

main :: IO ()
main = do
    print $ map (lineCheckMandelbrot 10) field

field :: Field
field = [[(x, y, False) | x <- toList (linspace 100 (-2, 2::Double))] | y <- toList (linspace 100 (-2, 2::Double))]

pair2complex :: Pos -> Complex Double
pair2complex (x, y, _) = x :+ y

c_f :: Complex Double -> Int -> Complex Double
c_f _ 0 = 0 :+ 0
c_f c n = ((c_f c (n-1)) ^ 2) + c

lineCheckMandelbrot :: Int -> [Pos] -> [Pos]
lineCheckMandelbrot n line = map (isMandelbrot n) line

isMandelbrot :: Int -> Pos -> Pos
isMandelbrot n (x, y, b) = (x, y, (isPointMandelbrot (x :+ y) n))

isPointMandelbrot :: Complex Double -> Int -> Bool
isPointMandelbrot c n = (magnitude (c_f c n)) < 2